namespace AlbelliTest.DAL.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AlbelliTest.DAL.Models.AlbelliContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AlbelliContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            if ((from q in context.Customer where q.CustomerId == 1 select q).Count() == 0)
            {
                Customer customer = new Customer();

                context.Customer.AddOrUpdate(
                                  p => p.CustomerId,
                                  new Models.Customer { Name = "TestCustomer", Email = "a@a.net", Orders = new List<Order> { new Order { Price = 1, CreatedDate = DateTime.Now } } });

            }

        }
    }
}
