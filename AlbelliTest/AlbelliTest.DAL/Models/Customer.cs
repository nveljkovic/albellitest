﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlbelliTest.DAL.Models
{
    public class Customer
    {
        public Customer()
        {
            this.Orders = new HashSet<Order>();
        }

        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}