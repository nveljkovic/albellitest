﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AlbelliTest.DAL.Models
{
    public class AlbelliContext : DbContext
    {
        public AlbelliContext() : base("name=AlbelliContext")
        {
            Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Order> Order { get; set; }

    }
}