﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AlbelliTest.DAL.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

    }
}