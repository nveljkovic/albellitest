﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using AlbelliTest.API.Server.Api;
using AlbelliTest.DAL.Models;

namespace AlbelliTest.Test
{
    /// <summary>
    /// Summary description for CustomerTest
    /// </summary>
    [TestClass]
    public class CustomerTest
    {
        public CustomerTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public async Task GetAllCustomers_ShouldReturnAllCustomers()
        {
            AlbelliContext db = new AlbelliContext();
            var customer = (from b in db.Customer select b).ToList();
            var controller = new CustomersController();

            var result = await controller.GetCustomer() as List<Customer>;
            Assert.AreEqual(customer.Count, result.Count);
        }

        [TestMethod]
        public async Task GetCustomer_ShouldNotFindCustomer()
        {
            var controller = new CustomersController();

            var result = await controller.GetCustomer(999);
            Assert.AreEqual(result.GetType().Name, "NotFoundResult");
        }

        [TestMethod]
        public async Task PostOrderWithoutCustomer_ShouldReturnConflictResult()
        {
            var testCustomers = GetTestCustomers();
            var testOrders = new Order
            {
                Price = 1,
                CreatedDate = DateTime.Now
            };
            var controller = new CustomersController();

            var result = await controller.PostCustomer(testOrders, 0);

            Assert.AreEqual(result.GetType().Name, "BadRequestErrorMessageResult");
        }


        private List<Customer> GetTestCustomers()
        {
            var testCustomers = new List<Customer>();
            testCustomers.Add(new Customer
            {
                Name = "TestCustomer",
                Email = "a@a.net",
                Orders = new List<Order> {
                    new Order { Price = 1, CreatedDate = DateTime.Now
                            },
                    new Order {
                        Price = 1,
                        CreatedDate =
                        DateTime.Now },
                    new Order {
                        Price = 1,
                        CreatedDate =
                        DateTime.Now
                    }
                }
            });

            return testCustomers;
        }
    }
}
