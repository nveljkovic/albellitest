﻿using AlbelliTest.API.Server.Startup;
using System.Web.Http;

namespace AlbelliTest.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiStartup.Configure);
        }
    }
}
