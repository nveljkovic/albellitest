﻿using Microsoft.Owin.Security.Jwt;
using System.Configuration;

namespace AlbelliTest.API.Server.Auth
{
    public class JwtOptions : JwtBearerAuthenticationOptions
    {
        public JwtOptions()
        {

            AllowedAudiences = new[] { ConfigurationManager.AppSettings["JwtAudience"] };
            IssuerSecurityTokenProviders = new[]
            {
                new SymmetricKeyIssuerSecurityTokenProvider(ConfigurationManager.AppSettings["JwtIssuer"]
                , ConfigurationManager.AppSettings["JwtKey"])
            };
        }
    }
}