﻿using System;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using System.Configuration;

namespace AlbelliTest.API.Server.Auth
{
    public class JwtWriterFormat : ISecureDataFormat<Microsoft.Owin.Security.AuthenticationTicket>
    {
        public string SignatureAlgorithm
        {
            get { return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256"; }
        }

        public string DigestAlgorithm
        {
            get { return "http://www.w3.org/2001/04/xmlenc#sha256"; }
        }

        public string Protect(AuthenticationTicket data)
        {

            if (data == null) throw new ArgumentNullException("data");

            var issuer = ConfigurationManager.AppSettings["JwtIssuer"];
            var audience = ConfigurationManager.AppSettings["JwtAudience"];
            var key = Convert.FromBase64String(ConfigurationManager.AppSettings["JwtKey"]);
            var now = DateTime.UtcNow;
            var expires = DateTime.UtcNow.AddHours(Convert.ToInt32(ConfigurationManager.AppSettings["tokenExpirationHours"]));
            var signingCredentials = new SigningCredentials(new InMemorySymmetricSecurityKey(key), SignatureAlgorithm,
                DigestAlgorithm);
            var token = new JwtSecurityToken(issuer, audience, data.Identity.Claims, now, expires, signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}