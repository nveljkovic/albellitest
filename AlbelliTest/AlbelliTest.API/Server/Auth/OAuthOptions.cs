﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using System.Configuration;

namespace AlbelliTest.API.Server.Auth
{
    public class OAuthOptions : OAuthAuthorizationServerOptions
    {
        public OAuthOptions()
        {
            TokenEndpointPath = new PathString(ConfigurationManager.AppSettings["TokenPath"]);
            AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["ExpirationMinutes"]));
            AccessTokenFormat = new JwtWriterFormat();
            Provider = new OAuthProvider();
            AllowInsecureHttp = true;
            //#if DEBUG
            //    AllowInsecureHttp = true;
            //#endif
        }
    }
}