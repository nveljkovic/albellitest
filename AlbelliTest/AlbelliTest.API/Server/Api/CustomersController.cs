﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AlbelliTest.DAL.Models;
using System.Collections.Generic;

namespace AlbelliTest.API.Server.Api
{
    [Authorize]
    public class CustomersController : ApiController
    {
        private AlbelliContext db = new AlbelliContext();

        // GET: V1/Customers
        [ResponseType(typeof(List<Customer>))]
        public async Task<List<Customer>> GetCustomer()
        {
            var customer = (from b in db.Customer select b);

            return await customer.ToListAsync();
        }

        // GET: V1/Customers/5
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            Customer customer = await (from b in db.Customer.Include(b => b.Orders).Where(x => x.CustomerId == id) select b).FirstOrDefaultAsync();

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // POST: V1/Customers
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Customer.Add(customer);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, customer);
        }

        // POST: V1/Customers/1/Orders
        [Route("~/V1/Customers/{customerid:int}/Orders", Name = "Post")]
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> PostCustomer(Order order, int customerid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if ((from q in db.Customer where q.CustomerId == customerid select q).Count() == 0)
            {
                return BadRequest("Customer doesn't exist!!!");
            }
            order.CustomerId = customerid;
            db.Order.Add(order);
            await db.SaveChangesAsync();

            return CreatedAtRoute("Post", new { id = order.OrderId }, order);
        }

    }
}