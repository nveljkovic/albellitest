﻿using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using AlbelliTest.API.Server.Startup;
using AlbelliTest.API.Server.Auth;

[assembly: OwinStartup(typeof(OwinStartup))]
namespace AlbelliTest.API.Server.Startup
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            app.UseJwtBearerAuthentication(new JwtOptions());
            app.UseOAuthAuthorizationServer(new OAuthOptions());
        }
    }
}