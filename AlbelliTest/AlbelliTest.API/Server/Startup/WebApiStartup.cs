﻿using Newtonsoft.Json.Serialization;
using System.Web.Http;

namespace AlbelliTest.API.Server.Startup
{
    public class WebApiStartup
    {
        public static void Configure(HttpConfiguration config)
        {

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            config.MapHttpAttributeRoutes();

            //config.MapODataServiceRoute(routeName: "ODataRoute", routePrefix: "odata", model: builder.GetEdmModel());

            config.Routes.MapHttpRoute("V1", "V1/{controller}/{id}", new { id = RouteParameter.Optional });
            config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "V1/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
            );
            //var json = config.Formatters.JsonFormatter;
            //json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            //config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}